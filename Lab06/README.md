# Lab. 6: Modulo de Compras.

## Desarrollo

1.1 Primero realizamos la instalacion del modulo de compras.

1.2. Siembargo tenemos un problema ya que al momento de escribir el precio con decimales este lo separa con coma no con punto como es es habital en peru pro eso tendremos que canbiarlo:

![contacto](Imagenes/1.JPG)

Podemos ver que al crear la solicitud de presupuesto se habilitan las opciones de Imprimir e incluso enviar por correo electrónico.

![contacto](Imagenes/2.JPG)

![contacto](Imagenes/3.JPG)

![contacto](Imagenes/4.JPG)

![contacto](Imagenes/5.JPG)

2.6. Validemos la Solicitud de presupuesto y veamos las nuevas opciones habilitadas. Se crea por defecto un envío (en caso tengamos productos que no sean servicios) y un botón para registro de las facturas del proveedor.

![contacto](Imagenes/6.JPG)

### Facturacion de proveedores

4. La factura ahora podrá ser encontrada dentro de la Solicitud de presupuesto o incluso dentro del menú de Facturación, submenú Compras, Facturas de Proveedor. Fíjese que aparece con un monto A pagar que es precisamente la deuda que tiene la empresa con el proveedor.

![contacto](Imagenes/8.JPG)

Si buscamos los detalles del proveedor (indique como llegó a este formulario) veremos que ahora aparece en su detalle una compra y una factura de proveedor

![contacto](Imagenes/9.JPG)

### Tarifas de proveedores

El Acuerdo de compra (o licitación) es muy parecido a la Solicitud de presupuesto, con la diferencia de que desde un inicio no se indica los precios a acordar ni el proveedor seleccionado. Cree una indicando una fecha límite y una fecha de entrega posteriores a la fecha actual, ya que serán las fechas hasta cuando los proveedores pueden enviar sus propuestas, y la fecha en que deben entregar preferiblemente sus propuestas.

![contacto](Imagenes/10.JPG)

![contacto](Imagenes/11.JPG)

Después de haber creado nuestra licitación, procedamos a Confirmarla. Veremos que se habilita la opción de crear un Nuevo Presupuesto. Estas serán las propuestas de nuestros proveedores.

![contacto](Imagenes/13.JPG)

Al volver a nuestra licitación, veremos cómo se contabilizan todas nuestras propuestas

![contacto](Imagenes/14.JPG)

### Módulos de terceros

Hay todo tipo de módulo, de todo rubro, y al mismo tiempo, algunos tendrán un costo y otros serán gratuitos.

![contacto](Imagenes/15.JPG)

Editaremos la línea con la variable llamada addons_path, le concatenaremos una coma (para separar ambas carpetas) y luego concatenaremos la ruta a nuestro directorio personalizado.

![contacto](Imagenes/16.JPG)

Como se puede observar aqui se muestra nuestro nuevo modulo que descargamos ya insertado en odoo.

![contacto](Imagenes/17.JPG)

## Conclusiones

- Hay que cambiar la configuracion de los decimales porque cuando queramos usarlo para precios este creara un problema de la cantidad real que quermos ingresar.

- Cuando queramos crear una entrega parcial Odoo, nos preguntará si deseamos confirmarla o darla por completa.

- En la pagina de odoo apps podemos acceder a una gran cantidad de modulos creados para ODOO estos tendran un costo o pueden ser gratuitos.

- Es recomendable crear una carpeta destinada solo a modulos de terceros y no insertarlo en las carpeta original que nos brinda ODOO para tener buenas contunbres.

- Odoo al momento de crear un modulo y utilizarlo no nos limita a solamente una o dos carpetas donde estén nuestros directorios.
