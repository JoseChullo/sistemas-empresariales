# Laboratorio 07: Modulo Punto de venta.

## Desarrollo

1. Primero realizamos la instalacion del modulo de punto de venta.

![contacto](Imagenes/1.JPG)

1.2 La interfaz de venta es relativamente fácil de utilizar. Automáticamente incluirá todos los productos que ya hayamos registrado en el sistema.

![contacto](Imagenes/2.JPG)

2. Así mismo, al dar click en el botón Pago, encontraremos una interfaz sencilla, donde elegiremos el medio de pago y demás condiciones. Por ahora nos aparecerá solamente con Efectivo (configuraremos más adelante otros medios como VISA o MASTERCARD)

3. Ingresado ya el medio de pago, de click en validar, aparecerá la impresión del ticket o comprobante de pago.

![contacto](Imagenes/6.JPG)

3.1 Vamos a dar click una vez en Cerrar y luego otra vez en Confirmar (esto evita que se cierre de manera accidental el punto de venta). Si nos dirigimos al listado de Sesiones y vemos la sesión actual veremos el total en efectivo de las transacciones del Punto de Venta. De esta manera tendremos un control de cierre de turno. Proceda a cerrar y validar esta sesión para iniciar otra con una nueva configuración.

![contacto](Imagenes/7.JPG)

4. El balance de Apertura se utiliza para declarar con cuanto trabaja inicialmente el cajero al comienzo del turno. Aquí se deberá ingresar todo el efectivo que se le entrega. Es una buena práctica entregar una cantidad estándar todos los días para facilitar este proceso.

![contacto](Imagenes/8.JPG)

5. Ahora sí, iniciaremos el Punto de Venta. Haga una venta que tenga dos medios de pago. Verá que Odoo permite pagar de distintas formas, con efectivo, con efectivo y otros medios combinado, solamente un medio bancarizado sin efectivo, etc. Añada imágenes de dichas ventas y comente las combinaciones que logró.

![contacto](Imagenes/9.JPG)

6. Intente acceder como Administrador. Verá que le solicita una contraseña, que justamente será la misma que colocamos en pasos anteriores. Verifique que ahora se habilite el botón de modificar precios.

![contacto](Imagenes/13.JPG)

![contacto](Imagenes/14.JPG)

![contacto](Imagenes/15.JPG)

7. Al entrar en la opción Diseño del Piso, crearemos dos pisos tal cual está indicado.

![contacto](Imagenes/16.JPG)

Esto se debe a que solamente se puede tener una sesión de punto de venta activa por usuario.

![contacto](Imagenes/17.JPG)

Ahora sí, ingrese al nuevo punto de venta. Verá las opciones de los pisos creados. Hagamos click en el lápiz ubicado a la derecha. Este nos servirá para dibujar las mesas del restaurante. Proceda a dibujarlas.

![contacto](Imagenes/19.JPG)
![contacto](Imagenes/Transferir.gif)

![contacto](Imagenes/Control de mesa.gif)

Podemos añadir notas

![contacto](Imagenes/20.JPG)

Volvamos a la configuración del Punto de Venta. Habilitemos la Impresión de cuenta y la separación de cuentas.

![contacto](Imagenes/21.JPG)

De igual manera, se habilitó el botón Dividir, que sirve para poder dividir el pago del consumo de la cuenta. De esta forma, se puede tomar el pedido de varias personas en una sola mesa y luego pagar y emitir comprobantes de pago por separado.

![contacto](Imagenes/21-22.JPG)

## Conclusiones

- Este Punto de Venta está pensado para ser utilizado en cualquier tipo de negocio desde minoristas hasta grandes empresas mayoristas
- Con este modulo podemos configurar los metodos de pagos que queramos utilizar para una tienda especifica ya sea efectivo, visa, mastercard.
- Es sumamente flexible y se puede adaptar a las necesidades de cualquier organización.
- Podemos configurar usuarios/cajeros con permisos por ejemplo se aplica a la hora de cambiar precios por que solo el administrador puede hacer este cambio.
