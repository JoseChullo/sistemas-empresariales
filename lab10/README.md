# Lab. 10: LLAMADAS A APIS EXTERNAS

## Desarrollo


### 2. Configuración inicial.

Proseguiremos modificando el módulo facturacion creado en el laboratorio anterior.

2. Creación de modelo Documentos
Crearemos el archivo documentos.py dentro de la carpeta models con el siguiente contenido:

![Contactos](Imagenes/1.JPG)


### 3. Relacion con Contactos

Crearemos el archivo res_partner.py dentro de la carpeta models con el siguiente contenido.
Lo que estamos haciendo es referenciar al modelo antes creado para saber qué tipo de documento tiene la entidad. Nos falta agregar dicho campo a las vistas.

![Contactos](Imagenes/2.gif)


### 4. Consumo de API externa

Ahora que tenemos el tipo de documento, haremos que el sistema complete los datos según si se escoge DNI o RUC. Ingresemos a la siguiente URL: https://www.apisperu.com/

En la parte inferior encontraremos un formulario para registrarnos. Utilice su correo de Tecsup para utilizar este servicio de validación de documentos.

![Contactos](Imagenes/3.JPG)

### 5. Constraint o limitantes.

5.1. Odoo también provee un método llamado constrains que restringe la creación de un contacto o su edición, en caso cumpla una condición. Por ejemplo, nosotros limitaremos la creación de dos contactos con el mismo documento.

5.2. Modificaremos el archivo res_partner.py para que tenga el siguiente nuevo método.
En caso de que el vat este vacío, no tendremos ningún problema. Pero en caso contrario, haremos una búsqueda de cuantos registros existen con dicho documento y tipo de documento. Para eso utilizaremos el método search_count y le pasaremos nuestras condiciones de búsqueda.

Las condiciones de búsqueda (o más conocidas como dominios) se suelen escribir en tres partes, haciendo referencia al campo a comparar, la operación (en este caso, = de comparación, pero puede ser < menor, > mayor, in de inclusión, not in de exclusión, etc.) para más detalles, puede consultarse la sección https://www.odoo.com/documentation/11.0/reference/orm.html#domains


## OBSERVACIONES Y CONCLUSIONES

- Nosotros podemos heredar vistas y modelos, para agregar campos, con la palabra _inherit

- Se presento varios tipos de errores a la hora de la creacion de la factura.

- Podemos consumir una api externa para obtener los respectivos nombres deacuerdo al DNI ingresado.

