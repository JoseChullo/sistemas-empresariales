from odoo import models, fields, api

class ResPartner(models.Model):
    _inherit = "res.partner"

    tipo_doc_id = fields.Many2one(comodel_name='facturacion.documentos', string='Tipo de documento')
    