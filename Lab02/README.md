# Lab. 2: Administracion de Modulos ODOO

## Desarrollo

1.1. Se hace una pequeña demostracion para filtrar,buscar, y/o configurar los modulos instalados.

1.2. Se nos pide que instalemos los tres modulos de Gestion de inventario, punto de venta, Gestion de ausensias.

![Gestion de invetario](Imagenes/1.1.JPG)
![Punto de venta](Imagenes/1.2.JPG)
![Gestion de ausensias](Imagenes/1.3.JPG)

1.3. La siguiente imagen muestra todos los modulos instalados.

![Full app](Imagenes/app.JPG)

2.1 De igual forma realizamos una instalacion en ubuntu:

![odoo server](Imagenes/odoo_server.JPG)

3.1. Activamos el mode desarrollador para poder crear nuestro modulo personalizado.

3.2. Se verifico que no exista la carpeta:

![Verificacion de carpeta](2.1.JPG)

## Tarea

- Elabore una descripcion breve de la funcionalidad que tiene cada uno de los elementos(la tabla esta desarrollada en el word).

## Conclusiones

- **Odoo **es un conjunto completo de apliaciones de negocio,incluyendo Ventas,Gestion de proyectos, Gestion de almacenes , etc.
- No sólo es un software fácil de usar, también es muy flexible. Odoo puede adaptarse a los requisitos empresariales específicos de su empresa.
- Odoo tiene modulos de premiun para sistemas empresariales.
- Se puede instalar una interface grafica en ubuntu server y todavia se pueden instalar app de escritoria.
