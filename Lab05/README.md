# Lab. 4:MODULO LOGISTICO

## Desarrollo

1.1. Primero realizamos la instalacion del modulo de inventario.

1.2. Crearemos el Almacén Secundario con código SEC. Este código o nombre corto es solamente una forma rápida de identificación:

Veremos una pantalla de edición de contacto.

![contacto](Imagenes/1.JPG)

2.-Gestion de producto

2.1 Al crear un producto viene seleccionada por defecto la opción de Producto Almacenable.

2.2 Crearemos el producto manzana roja con los siguientes datos.

![manzana roja](Imagenes/2.JPG)

3.- Importancion y exportacion masiva de productos

3.1. En este punto nos concentraremos en como importar dichos productos a una plantilla de excel, seleccionaremos todos los productos y aparecerá una opción llamada Acción, dentro de la cual encontraremos Exportar..

![Excel](Imagenes/3.JPG)

Agregamos mas productos en esta plantilla:

![Mas productos](Imagenes/4.JPG)

Y luego importamos el documento guardado el resultado es el siguiente.

![IMportacion](Imagenes/5.JPG)

4.- Ajustes de inventarios.

Hemos creado nuestros productos a través del importador pero no tienen stock por este motivo tenemos que modificar el inventario.

![Inventario](Imagenes/6.JPG)

Al volver a la vista de productos, veremos reflejado el ajuste al ver los nuevos stocks.

![Inventario modificado](Imagenes/7.JPG)

5.-Transferencias internas.

Esta opcion nos brinda informacion de todos cambios que ocurren y quien realizo este cambio en el inventario.

![Inventario modificado2](Imagenes/8.JPG)

Podemos Observar que el stock de platano se dividio en dos uno el principal y otro en el almacenamiento secundario.

![Stock modificado](Imagenes/9.JPG)

## Tarea

- Habilite las opciones Atributos y variantes, y Unidades de medida. Vaya a la ficha de producto e indique las diferencias, así como adjunte imágenes de productos con varios atributos y/o unidades de medida distintas.

## Conclusiones

-Se creo una nueva base de datos sin datos de ejemplo(en mi caso) para no confundirse con los productos que esta creando con los de defecto.

- Al crear un producto viene seleccionada por defecto la opción de Producto Almacenable.
- La ventana de exportar nos muestra todas las vistas tipo del sistema , no solamente en la vista de productos , podemos exportado de tipo CSV o Excel.
  -Odoo por defecto gestiona solamente una ubicación de almacenamiento por este motivo tenemos que configurarlo para que sea de modo multi almacenes.
- La opción de Movimientos de producto, nos permite rastrear cada traslado de dicho producto ya sea entre almacenes o salidas del mismo.
