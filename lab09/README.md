# Laboratorio 09: Módulo Básico y Herencia.

## Desarrollo

1. Primero realizamos la instalacion del modulo de punto de venta.

1-1 Crearemos nuestra carpeta facturacion dentro de nuestra carpeta de addons. Recuerde que esta carpeta debe ser una carpeta externa a la instalación de Odoo, tal como se configuró en laboratorios anteriores para instalar módulos de terceros. Dentro de dicha carpeta crearemos la carpeta static. Dentro de la misma, la carpeta description y finalmente dentro, pegaremos el archivo icon.png.

![contacto](Imagenes/1.JPG)

### Vista Formulario 
2. Dentro del archivo series_view.xml agregaremos un record para crear la vista de formulario.
Actualizaremos nuestro módulo (al solamente haber modificado xml, no se necesita recompilar Python, por lo que no se necesita reiniciar el servicio) y adjuntaremos un GIF del funcionamiento de nuestro listado con su nuevo formulario.

![contacto](Imagenes/Transferir.gif)


### Vista Busqueda
3. Ingresado ya el medio de pago, de click en validar, aparecerá la impresión del ticket o comprobante de pago.

![contacto](Imagenes/prefijogif.gif)

### Data por defecto

4. En muchas ocasiones, necesitaremos importar data por defecto. Para lograr esto, modificaremos nuestro archivo __manifest__.py para agregar la línea data/series.xml

![contacto](Imagenes/3.JPG)

### Verificación de API automática
5. Ahora, haremos una pausa en el desarrollo y verificaremos que Odoo crea por nosotros una API de consulta del modelo ya creado sin necesidad de hacer alguna configuración extra. Crearemos una carpeta llamada node con el siguiente package.json. Luego haremos npm install para instalar las dependencias

![contacto](Imagenes/pruyeba_api.gif)

## Conclusiones

- En esta laboratorio pudimos crear un modelo perzonalizado con herencia.
- Tambien creamos una api que nos brindara los dato por defecto y lo mostremos en el formulario.
- La Carpeta en la que creamos el modelo facturacion de estar dentro de las carpetas de tus modelos por terceros.

