# Lab. 3:CRM EN ODOO

## Desarrollo

1.1. Primero realizamos la instalacion del modulo de CRM.

1.2. Despues de entrar a la configuracion de CRM nos pedira serie de opciones para personalizarlo y al finalizar se nos mostrara de esta forma:

![Enhorabuena, ha terminado](Imagenes/1.1.JPG)

1.3. Tambien configuramos CRM - Clientes desde el tablero ingresamos a clientes para crear uno nuevo debe quedar dela siguiente forma.

![Creacion de cliente acme](Imagenes/4.10.JPG)

2.1 Editamos el Equipo de ventas directas agregnado nuevo mienbros a la lista

![mienbros de equipo](Imagenes/5.JPG)

3.1. Iniciativas y/o oporunidades en Odoo verificamos el estado y creamos una nueva iniciativa.

![Estado de iniciativa](Imagenes/8.JPG)

Mostramos el nuevo contacto:

![Contacto generado](Imagenes/6.JPG)

![Flujo de ventas](Imagenes/7.JPG)

3.2. Creamos una nueva oportunidad, en el tablero entramos en flujo de trabajo y activamos el kanban despues de una serie de configuraciones nos debe quedar de la siguiente forma:

![Nueva oportunidad](Imagenes/9.JPG)

4.- Generacion del sitio web con el modulo "website builer", seleccionamos uno de los siguientes temas

![Temas](Imagenes/temas.JPG)

Tambien realizamos la instacion de otro modulo para formularios

![modulo](Imagenes/ga.JPG)

el cual tiene el siguiente diseño.

![formulario](Imagenes/10.JPG)

Agregando nuevas oportunidades:

![formulario1](Imagenes/form1.JPG)

## Tarea

- Describa el procedimiento para importar contactos/clientes dede un archivo CSV.

Nos diregimos al area de contactos y selecionamos importar aqui seleccionamos el archivo scv hay que tener en cuenta que hay que añade, eliminar y organizar columnas para adaptarse a la estructura de la información porque es posible que Odoo no pueda mapearlo automáticamente si la etiqueta no corresponde a ningún campo en el sistema.

-Mejore la Presentacion del modulo Website instalado adicionando mayor cantidad de elementos disponibles en su editor

## Conclusiones

- Una iniciativa se le adquire del contacto y recolectamos informacion importante sobre los clientes con el fin de tener una buena relacion.
- Como se dijo anteriormente podemos configurar Odoo sin necesidad de escribir codigo a no ser que requieras una accion muy especifica.
- Se realizo una serie de configuraciones a lo largo del laboratorio con el fin de familiarizarse con el entorno de ODOO y como funciona generalmente.
- Podemos convertir una iniciativa a una oportunida y seleccionar que empleado estara a su cargo con la informacion que recibimos del formulario o crearlo manualmente.
